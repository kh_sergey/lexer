(function(trans) {
  /**
   * Это лексический анализатор. Здесь должен быть ваш код вместо данной затычки!
   * @param {string} inputCode - строка с кодом программы на входном языке
   * @return {Array} - массив распознанных лексем
   */
  //var inputCode = "mutable if (a == b) {";

  trans.TransProto.lexer = function (inputCode) {

    var inputCode = "program var a:integer";
    var lex = this.lexTable; //Таблица лексем
    var const_tab = new Array();
    var const_tab = [];
    var d = 0;
    var c = 0;


    // Обнуляем массив с индетификаторами и константами
    //for(var i in this.arrayOfSymbols){
    //  this.arrayOfSymbols[i] = [];
    //}

    var cur_str = 1; //Счетчик строк, для ошибок
    var cur_colon = 1 //Счетчик столбцов

    while(inputCode.length != 0){
      var flag = false; //Флаг, что лексема найдена
      for (var key in lex){

        //Создаем регулярное выражение
        var reg = new RegExp(lex[key].regex);
        var match = reg.exec(inputCode); //проверяем на совпадение с регуляркой

        //Если найдена лексема
        if(match && match.length != 0){
          ///////////////////////////////////////
          //*****Нужно ли пропустить лексему*****//
          if(lex[key].skip){
            flag =true;
            inputCode = inputCode.replace(reg, "");//Удаляем пробелы
            // cur_colon+=match[0].length;

            //Если конец строки, запоминаем строкуномер строки
            for (var i=0; i< match[0].length; i++)
              if(match[0][i] == "\n"){
                cur_str++;
                cur_colon=-1;

                //нужно ли в конце строки добавлять ";" в список лексем ?
              }
          } else if(lex[key].list){  // Здесь надо найти к какой именно лексеме он относится
            //Определяем какая именно лексема
            for (var nameList in lex[key].list){
              for (var subname in lex[key].list[nameList]){

                var name_lex = lex[key].list[nameList][subname];//Помещаем n-ю лексему из таблицы лексем в перемену
                //Найденная лексема совпала совпала с лексемой из таблицы
                console.log(match[0]);
                if(name_lex == match[0]){
                  flag = true;

                  this.lexArray.push([key, name_lex]); //Заносим в список лексем

                  inputCode = inputCode.replace(reg, ""); //Удаляем из исходного текста найденую лексему
                  break;
                }
              }
            }
          }else if (key == "ident" || key == "digit_const" || key == "string_const"){  //значит это константа или индификатор
            var val = match[0].toString();


            if (isNaN(Number(val))) {
                this.arrayOfSymbols["arrayOfIdent"][d] = new Array();
                this.arrayOfSymbols["arrayOfIdent"][d].push(d, match[0]);
                this.lexArray.push(["ident", "string_const", match[0]])
                d++;
            } else {
                this.arrayOfSymbols["arrayOfConst"][c] = new Array();
                this.arrayOfSymbols["arrayOfConst"][c].push(c, match[0]);
                this.lexArray.push(["ident", "digit_const", match[0]])
                c++;
            }


            flag = true;
            inputCode = inputCode.replace(reg, ""); //Удаляем лексему из текста, переходим к следующей
          }
        }
        //Лексема не найдена, проверяем следующую регуряку
        if(flag){
          cur_colon+=match[0].length;
          break;
        }
      }

      /*
        Ниже идет вывод сообщения о том что не удалось распознать
        лексему и занесения не распознанной лексемы в
        массив ошибок
      */

      if(!flag){
        console.log("Error in line " + cur_str+", colon "+cur_colon); //Вывод номера строки где не удалось распознать лексему
        console.log(inputCode);//Вывод кода, начиная с нераспознанной лексемы

        //занесения не распознанной лексемы в массив ошибок
        var lex_error = inputCode.split(/\s+/);
        this.errors.push(lex_error[0], cur_str, cur_colon);

        inputCode = inputCode.replace(lex_error[0], "");
      }
    }

    //Возвращаем объект, с массивами лексем, констант, нераспознанных лексем
    console.log(this.lexArray);

    return {
      "Lexems": this.lexArray,
      "Constants": this.arrayOfSymbols,
      "Error lexems": this.errors
    };
  }

})(window.trans || (window.trans = {}));